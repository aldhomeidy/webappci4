<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="<?= base_url('/vendor/bootstrap-5.0.1/css/bootstrap.min.css') ?>">
</head>

<body>
    <?= $this->renderSection('content'); ?>
</body>

<script src="<?= base_url('/vendor/bootstrap-5.0.1/js/popper.min.js') ?>"></script>
<script src="<?= base_url('/vendor/bootstrap-5.0.1/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('/vendor/bootstrap-5.0.1/js/jquery-3.6.0.min.js') ?>"></script>

</html>