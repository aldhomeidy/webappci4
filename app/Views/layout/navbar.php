<?php
$uri = service('uri');
$segment = $uri->getSegment(2);

$master = ['', '', '', ''];

if ($segment == "menu") {
    $master[0] = 'active';
} else if ($segment == "kategori") {
    $master[1] = 'active';
} else if ($segment == "room") {
    $master[2] = 'active';
} else {
    $master[3] = 'active';
}
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="javascript:void(0)">
            <img src="<?= base_url('/assets/pict/metrodata.png') ?>" alt="Resto" width="50" style="margin: 0 0 ;">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link <?= $master[0] ?>" aria-current="page" href="<?= base_url('/master/menu') ?>">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $master[1] ?>" href="<?= base_url('/master/kategori') ?>">Kategori Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $master[2] ?>" href="<?= base_url('/master/room') ?>">Rooms</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $master[3] ?>" href="<?= base_url('/master/user') ?>">User</a>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-light" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>