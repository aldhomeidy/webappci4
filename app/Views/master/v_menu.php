<?php
$uri = service('uri');
$segment = $uri->getSegment(2);
$num = 1;
?>

<?= $this->extend('layout/templates') ?>

<?= $this->section('content'); ?>
<div class="container-fluid mt-2">

  <?= $this->include('layout/navbar') ?>

  <div class="content mt-2">
    <table class="table table-hover table-striped table-dark">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Menu</th>
          <th scope="col">Kategori</th>
          <th scope="col">Harga</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $rs) : ?>
          <tr>
            <th scope="row"><?= $num++ ?></th>
            <td><?= $rs['menu'] ?></td>
            <td><?= $rs['kategori'] ?></td>
            <td><?= $rs['harga'] ?></td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</div>
<?= $this->endSection(); ?>