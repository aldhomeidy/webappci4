<?= $this->extend('layout/templates') ?>
<!-- extend() untuk menggunakan file view layout yang sudah dibuat -->

<?= $this->section('content'); ?>
<!--section() untuk membuat sebuah section yang akan digunakan dari view layout yang di load. Jangan lupa ditutup dengan endSection() -->

<div class="container-fluid mt-2">

    <?= $this->include('layout/navbar') ?>
    <!--include() untuk menyisipkan sebuah file view kedalam file view -->

    <div class="content mt-2">
        <?php
        $attributes = ['class' => 'row g-3', 'method' => 'post'];
        echo form_open('master/menu/save', $attributes);
        ?>
        <div class="col-md-6">
            <label for="namaMenu" class="form-label">Nama Menu</label>
            <input type="text" class="form-control" id="namaMenu" name="menu">
        </div>
        <div class="col-md-2">
            <label for="harga" class="form-label">Harga</label>
            <input type="text" class="form-control" id="harga" name="harga">
        </div>
        <div class="col-md-4">
            <label for="kategori" class="form-label">Kategori</label>
            <select id="kategori" class="form-select" name="kategori">
                <?php foreach ($kategori as $kg) : ?>
                    <option value="<?= $kg['id'] ?>"><?= $kg['kategori'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-dark">Save</button>
        </div>
        <?= form_close() ?>
    </div>
</div>
<?= $this->endSection(); ?>