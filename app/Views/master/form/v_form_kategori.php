<?php
echo $this->extend('layout/templates'); //extend() untuk menggunakan file view layout yang sudah dibuat
$session = \Config\Services::session(); //inisiasi untuk menggunakan session didalam view
?>

<?= $this->section('content'); ?>
<!--section() untuk membuat sebuah section yang akan digunakan dari view layout yang di load. Jangan lupa ditutup dengan endSection() -->

<div class="container-fluid mt-2">

    <?= $this->include('layout/navbar') ?>
    <!--include() untuk menyisipkan sebuah file view kedalam file view -->

    <div class="content mt-2">
        <?php if ($session->getFlashdata('result') != null) : ?>
            <div class="alert alert-<?= $session->getFlashdata('status') ?>" role="alert">
                <?= $session->getFlashData('message') ?>
            </div>
        <?php endif ?>
        <?php
        $attributes = ['class' => 'row g-3', 'method' => 'post'];
        echo form_open('master/kategori/save', $attributes);
        ?>

        <div class="col-6">
            <label for="kategori" class="form-label">Kategori</label>
            <input type="text" class="form-control" name="kategori" autocomplete="off" placeholder="Kategori Menu">
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-dark">Save</button>
        </div>
        <?= form_close() ?>
    </div>
</div>
<?= $this->endSection(); ?>