<?php

namespace App\Controllers;

use App\Models\UserModel;

class Resto extends BaseController
{

    protected $userModel;

    public function __construct(){
        $this->userModel = new UserModel();
    }

	public function index()
	{       
        dd($this->userModel->getDataById('0621001'));
	}
    
    public function all()
    {
        dd($this->userModel->getAllData());        
    }
    
    public function save()
    {
        dd($this->userModel->saveData());
    }
    public function update()
    {        
        dd($this->userModel->updateData('0621003'));
    }
    public function delete()
    {        
        dd($this->userModel->deleteData('0621003'));
    }
    public function getByRole()
    {
        dd($this->userModel->getDataByRole());        
    }
}
