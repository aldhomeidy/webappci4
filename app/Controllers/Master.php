<?php

namespace App\Controllers;

use App\Models\KategoriMenuModel;
use App\Models\MenuModel;

class Master extends BaseController
{
    protected $kategoriModel;

    public function __construct()
    {
        $this->kategoriModel = new KategoriMenuModel();
        $this->menuModel = new MenuModel();
    }

    public function index($master, $action = "get", $id = false)
    {
        if ($master == "kategori") {
            $this->kategoriMenu($action, $id);
        } else if ($master == "menu") {
            $this->menu($action, $id);
        } else if ($master == "user") {
            echo "Jalankan method master user";
        } else if ($master == "room") {
            echo "Jalankan method master room";
        } else {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    protected function kategoriMenu($action = "get", $id = false)
    {
        $title = "Master Data | Kategori";
        switch ($action) {
            case "get":
                $result = $this->kategoriModel->getData($id);
                $data = [
                    'title' => $title,
                    'result' => $result
                ];
                echo view('master/v_kategori', $data);
                break;
            case "save":
                $dataSave = ['kategori' => $this->request->getPost('kategori')];
                if ($this->kategoriModel->insertData($dataSave)) {
                    $flashData = ['status' => 'success', 'message' => 'Data berhasil di simpan'];
                } else {
                    $flashData = ['status' => 'danger', 'message' => 'Data tidak berhasil di simpan'];
                }
                $this->session->setFlashdata($flashData);
                // dd($this->session->getFlashdata('message')); //silahkan di uncomment untuk melihat apakah flashdata sudah berhasil di set
                return redirect()->to(base_url('/master/form/kategori'));  //tidak bisa. yuk solving bareng :)    
                // header("Location: http://localhost:8080/master/form/kategori"); //pakai method dari php juga gabisa
                break;
            case "update":
                $data = [
                    'kategori' =>  'Snack',
                    'id'       => $id
                ];
                if ($this->kategoriModel->updateData($data)) {
                    echo "Update berhasil";
                } else {
                    echo "Update gagal";
                }
                break;
            case "delete":
                if ($this->kategoriModel->deleteData($id)) {
                    echo "Delete berhasil";
                } else {
                    echo "Delete gagal";
                }
                break;
            default:
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }
    protected function menu($action = "get", $id = false)
    {

        $title = "Master Data | Daftar Menu";
        switch ($action) {
            case "get":
                $result = $this->menuModel->getData($id);
                $data = [
                    'title' => $title,
                    'result' => $result
                ];
                echo view('master/v_menu', $data);
                break;
            case "save":
                $dataSave = [
                    'id_kategori' => $this->request->getPost('kategori'),
                    'nama_menu' => $this->request->getPost('menu'),
                    'harga' => $this->request->getPost('harga')
                ];
                if ($this->menuModel->insertData($dataSave)) {
                    echo "Save berhasil";
                } else {
                    echo "Save gagal";
                }
                break;
            case "update":
                $data = ['id_kategori' => 2, 'nama_menu' => 'Lemon Squash', 'harga' => 17000, 'id' => 2];
                if ($this->menuModel->updateData($data)) {
                    echo "Update berhasil";
                } else {
                    echo "Update gagal";
                }
                break;
            case "delete":
                if (!$this->menuModel->deleteData($id)) {
                    echo "Delete Berhasil";
                } else {
                    echo "Delete Gagal";
                }
                break;
            default:
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    public function formView($master)
    {
        if ($master == 'kategori') {
            $data = ['title' => 'Form | Master Kategori'];
            echo view('master/form/v_form_kategori', $data);
        } else if ($master == 'menu') {
            $data = [
                'title' => 'Form | Master Menu',
                'kategori' => $this->kategoriModel->getData(false)
            ];
            echo view('master/form/v_form_menu', $data);
        } else if ($master = 'room') {
            echo "menampilkan form room";
        } else if ($master = 'user') {
            echo "menampilkan form user";
        } else {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }
}
