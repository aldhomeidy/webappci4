<?php

namespace App\Models;

use CodeIgniter\Model;

class KategoriMenuModel extends Model
{
    protected $table = 'kategori_menu';
    protected $returnType = 'array';
    protected $allowedFields = ['kategori'];
    protected $useAutoIncrement = true;    

    protected $db;
    protected $builder;

    public function __construct()
    {
        // $this->db = \Config\Database::connect(); // ini cara kedua untuk mendapatkan koneksi ke db       
        $this->db = db_connect(); //ini cara pertama untuk mendapatkan koneksi ke db
        $this->builder = $this->db->table($this->table);   
    }

    //retrieve data
    public function getData($id=false)
    {
        if($id!=false)
        {
            return $this->builder->getWhere(['id' => $id])->getResult('array');
        }
        return $this->builder->get()->getResult('array');
    }

    //insert data
    public function insertData($insertedData)
    {
        $data = [
            'kategori' =>  $insertedData['kategori']           
        ];
        
        return $this->builder->insert($insertedData);    
    }
    
    //update data
    public function updateData($updatedData)
    {
        $data = [
            'kategori' =>  $updatedData['kategori']           
        ];
        $this->builder->where('id',$updatedData['id']);
        return $this->builder->update($data);    
    }

    //delete data
    public function deleteData($id)
    {
        $this->builder->where('id', $id);
        return $this->builder->delete();
    }
}