<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'user';
    protected $returnType = 'array';
    protected $allowedFields = ['id','nama','username','role','email','no_hp','password'];
    protected $useSoftDeletes = true;
    protected $useTimestamps = true;    

    protected $db;    
    protected $builder;  

    public function __construct()
    {
        // $this->db = db_connect(); //ini cara pertama untuk mendapatkan koneksi ke db
        $this->db = \Config\Database::connect(); // ini cara kedua untuk mendapatkan koneksi ke db       
        // $this->builder = $this->db->table($this->table);   
    }

    public function getAllData()
    {
        // return $this->withDeleted()->findAll();
        return $this->db->query("SELECT * FROM user")->getResult('array');                
    }

    public function getDataById($id)
    {
        return $this->find($id);
    }

    public function getDataByRole()
    {
        $sql = "SELECT * FROM user WHERE role = ? AND password = ?";
        return $this->db->query($sql, ['Staff', 'test'])->getResult('array');
    }

    public function saveData(){        
        $data = [
            'id' => '0621004',
            'nama' => 'Ellon Musk',
            'username' => 'ellon',
            'email' => 'ellon@gmail.com',
            'password'=>'test',
            'no_hp'=>'00000000000'            
        ];
        return $this->insert($data);
    }

    public function updateData($id)
    {
        $data = [                        
            'password'=>'test'            
        ];
        return $this->update($id,$data);
    }

    public function deleteData($id)
    {
        return $this->delete($id);
    }    
}