<?php

namespace App\Models;

use CodeIgniter\Model;

class MenuModel extends Model
{
    protected $table = 'menu';
    protected $returnType = 'array';
    protected $allowedFields = ['nama_menu', 'harga', 'id_kategori'];
    protected $useAutoIncrement = true;

    protected $db;
    protected $builder;

    public function __construct()
    {
        // $this->db = \Config\Database::connect(); // ini cara kedua untuk mendapatkan koneksi ke db       
        $this->db = db_connect(); //ini cara pertama untuk mendapatkan koneksi ke db
        $this->builder = $this->db->table($this->table);
    }

    //retrieve data
    public function getData($id)
    {
        $this->builder->select('menu.nama_menu as menu,kg.kategori, harga');
        $this->builder->join('kategori_menu as kg', 'kg.id = menu.id_kategori');

        if ($id <> false) {
            $this->builder->where('menu.id', $id);
        }

        return $this->builder->get()->getResult('array');
    }

    //insert data
    public function insertData($insertedData)
    {
        $data = [
            'id_kategori' => $insertedData['id_kategori'],
            'nama_menu' => $insertedData['nama_menu'],
            'harga' => $insertedData['harga'],
        ];

        return $this->builder->insert($data);
    }

    //update data
    public function updateData($updatedData)
    {
        $data = [
            'id_kategori' => $updatedData['id_kategori'],
            'nama_menu' => $updatedData['nama_menu'],
            'harga' => $updatedData['harga'],
        ];
        $this->builder->where('id', $updatedData['id']); //cara 1
        // $this->builder->where(['id'=>$updatedData['id']]); //cara 2

        return $this->builder->update($data);
    }

    //delete data
    public function deleteData($id)
    {
        $this->builder->where('id', $id); //cara 1
        $result = $this->builder->delete(); //cara1 

        // $result =  $this->builder->delete(['id' => $id]); //cara 2

        return $result;
    }
}
